import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HotelsComponent } from './hotels/hotels.component';
import { VoyagesComponent } from './voyages/voyages.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';
import { NavbarComponent } from './navbar/navbar.component';


const routes: Routes = [
  { path:'hotels', component: HotelsComponent},
  {path:'login', component: LoginComponent},
  {path:'admin', component: AdminComponent},
  { path:'voyages', component: VoyagesComponent},
  {path:'navbar', component: NavbarComponent},
  {path:'test', redirectTo:'/hotels'},
  { path:'**', component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
