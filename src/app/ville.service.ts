import { Injectable } from '@angular/core';
import {Ville} from './ville';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VilleService {
  private villesUrl= "https://www.my-freedomtravel.com/api/villes.json";
  constructor(private http:HttpClient){}
  getVilles():Observable<Ville[]>{
    return this.http.get<Ville[]>(this.villesUrl)
  }
  
}