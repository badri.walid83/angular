import { Component, OnInit } from '@angular/core';
import {Ville} from '../ville';
import {VilleService} from '../ville.service';

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelsComponent implements OnInit {
villes:Ville[];
  constructor(private VilleService:VilleService) { }

  ngOnInit() {this.getVilles();
  }
  getVilles():void { this.VilleService.getVilles()
    .subscribe(villes=>
      {
        this.villes=villes,
        console.log(villes)
      },
      err=>console.log(err)
    );
      
  }

}
